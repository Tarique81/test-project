import { KhelaHbePage } from './app.po';

describe('khela-hbe App', function() {
  let page: KhelaHbePage;

  beforeEach(() => {
    page = new KhelaHbePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
